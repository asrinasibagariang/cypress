/// <reference types="cypress" />

describe("Basic Test", () => {
  it("We have correct title", () => {
    cy.visit("https://codedamn.com");
    cy.contains("Build projects, practice and learn to code from scratch - without leaving your browser.");
  });
});
